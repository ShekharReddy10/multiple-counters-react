import React, { Component } from 'react'
import './styles.css'

class ChildCounter extends Component {
    render() {

        return (
            <div className='count'>
                <div>{this.props.counter.value}</div>
                <button onClick={() => this.props.onIncrement(this.props.counter)}>+</button>
            </div>
        )
    }
}

export default ChildCounter