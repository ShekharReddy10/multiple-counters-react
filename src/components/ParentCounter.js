import React, { Component } from 'react'
import ChildCounter from './ChildCounter'
import './styles.css'

class ParentCounter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            counters: [],
            id: 0
        }
    }
    incrementCounters = () => {
        this.setState({
            id: this.state.id + 1,
            counters: [...this.state.counters, { id: this.state.id, value: 0 }]
        })
    }
    resetHandler = () => {
        let counters = this.state.counters.map(counter => {
            counter.value = 0
            return counter
        })
        this.setState({ counters })
    }
    incrementHandler = (counter) => {
        let counters = [...this.state.counters]
        let index = counters.indexOf(counter)
        counters[index] = { ...counter }
        counters[index].value++
        this.setState({ counters })
    }
    render() {
        return (
            <div >
                <button onClick={this.incrementCounters} >Add Counter</button>
                <div className='count'>
                    {this.state.counters.map((counter) => {
                        return <ChildCounter key={counter.id} counter={counter} onIncrement={this.incrementHandler}></ChildCounter>
                    })}
                </div>
                <button onClick={this.resetHandler} variant="danger">Reset Counters</button>
            </div>
        )
    }
}

export default ParentCounter