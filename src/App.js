import React, { Component } from 'react';
import './App.css';
import ParentCounter from './components/ParentCounter';

class App extends Component {
  render() {
    return(
    <div className="App">
      <ParentCounter />
    </div>
    )
  };
}

export default App;
